const express = require('express');
const router = express.Router();
const saucesControl = require('../controllers/sauces')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer.config')

// lien vers router pour créer une sauce
router.post('/', auth, multer, saucesControl.createSauce );

// lien vers router pour modifier une sauce
router.put('/:id', auth, multer, saucesControl.modifSauce);

// lien vers router pour supprimer une sauce
router.delete('/:id', auth, saucesControl.deleteSauce);

// lien vers router pour selectionner une sauce
router.get('/:id', auth, saucesControl.getOneSauce);

// lien vers router pour selectionner toutes les sauces
router.get('/', auth,  saucesControl.getAllSauce);

// liens vers le router pour gerer les likes
router.post('/:id/like', auth, saucesControl.likeDislike)

module.exports = router;